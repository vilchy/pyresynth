# SPDX-FileCopyrightText: 2001-2002 Enthought
# SPDX-FileCopyrightText: 2003-2024 SciPy Developers
#
# SPDX-License-Identifier: BSD-3-Clause

from _typeshed import Incomplete
from typing import List, Literal
import numpy.typing as npt

def sawtooth(t, width: int = 1): ...
def square(t: npt.ArrayLike, duty: npt.ArrayLike = 0.5) -> npt.NDArray: ...
def gausspulse(
    t,
    fc: int = 1000,
    bw: float = 0.5,
    bwr: int = -6,
    tpr: int = -60,
    retquad: bool = False,
    retenv: bool = False,
): ...
def chirp(
    t: npt.ArrayLike,
    f0: float,
    t1: float,
    f1: float,
    method: Literal["linear", "quadratic", "logarithmic", "hyperbolic"] = "linear",
    phi: int = 0,
    vertex_zero: bool = True,
) -> npt.NDArray: ...
def sweep_poly(t, poly, phi: int = 0): ...
def unit_impulse(shape, idx: Incomplete | None = None, dtype=...): ...
