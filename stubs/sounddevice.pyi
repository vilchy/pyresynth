# SPDX-FileCopyrightText: 2015-2024 Matthias Geier
#
# SPDX-License-Identifier: MIT

# Copied and adapted from:
# https://github.com/spatialaudio/python-sounddevice/blob/master/sounddevice.py

from typing import Any
from _typeshed import Incomplete
import numpy.typing as npt

__version__: str

def play(
    data: npt.ArrayLike,
    samplerate: int | float | None = ...,
    mapping: npt.ArrayLike | None = ...,
    blocking: bool = ...,
    loop: bool = ...,
    **kwargs: Any
) -> None: ...
def rec(
    frames: Incomplete | None = None,
    samplerate: Incomplete | None = None,
    channels: Incomplete | None = None,
    dtype: Incomplete | None = None,
    out: Incomplete | None = None,
    mapping: Incomplete | None = None,
    blocking: bool = False,
    **kwargs
): ...
def playrec(
    data,
    samplerate: Incomplete | None = None,
    channels: Incomplete | None = None,
    dtype: Incomplete | None = None,
    out: Incomplete | None = None,
    input_mapping: Incomplete | None = None,
    output_mapping: Incomplete | None = None,
    blocking: bool = False,
    **kwargs
): ...
def wait(ignore_errors: bool = True): ...
def stop(ignore_errors: bool = True) -> None: ...
def get_status(): ...
def get_stream(): ...
def query_devices(device: Incomplete | None = None, kind: Incomplete | None = None): ...
def query_hostapis(index: Incomplete | None = None): ...
def check_input_settings(
    device: Incomplete | None = None,
    channels: Incomplete | None = None,
    dtype: Incomplete | None = None,
    extra_settings: Incomplete | None = None,
    samplerate: Incomplete | None = None,
) -> None: ...
def check_output_settings(
    device: Incomplete | None = None,
    channels: Incomplete | None = None,
    dtype: Incomplete | None = None,
    extra_settings: Incomplete | None = None,
    samplerate: Incomplete | None = None,
) -> None: ...
def sleep(msec) -> None: ...
def get_portaudio_version(): ...

class _StreamBase:
    def __init__(
        self,
        kind,
        samplerate: Incomplete | None = None,
        blocksize: Incomplete | None = None,
        device: Incomplete | None = None,
        channels: Incomplete | None = None,
        dtype: Incomplete | None = None,
        latency: Incomplete | None = None,
        extra_settings: Incomplete | None = None,
        callback: Incomplete | None = None,
        finished_callback: Incomplete | None = None,
        clip_off: Incomplete | None = None,
        dither_off: Incomplete | None = None,
        never_drop_input: Incomplete | None = None,
        prime_output_buffers_using_stream_callback: Incomplete | None = None,
        userdata: Incomplete | None = None,
        wrap_callback: Incomplete | None = None,
    ) -> None: ...
    @property
    def samplerate(self): ...
    @property
    def blocksize(self): ...
    @property
    def device(self): ...
    @property
    def channels(self): ...
    @property
    def dtype(self): ...
    @property
    def samplesize(self): ...
    @property
    def latency(self): ...
    @property
    def active(self): ...
    @property
    def stopped(self): ...
    @property
    def closed(self): ...
    @property
    def time(self): ...
    @property
    def cpu_load(self): ...
    def __enter__(self): ...
    def __exit__(self, *args) -> None: ...
    def start(self) -> None: ...
    def stop(self, ignore_errors: bool = True) -> None: ...
    def abort(self, ignore_errors: bool = True) -> None: ...
    def close(self, ignore_errors: bool = True) -> None: ...

class RawInputStream(_StreamBase):
    def __init__(
        self,
        samplerate: Incomplete | None = None,
        blocksize: Incomplete | None = None,
        device: Incomplete | None = None,
        channels: Incomplete | None = None,
        dtype: Incomplete | None = None,
        latency: Incomplete | None = None,
        extra_settings: Incomplete | None = None,
        callback: Incomplete | None = None,
        finished_callback: Incomplete | None = None,
        clip_off: Incomplete | None = None,
        dither_off: Incomplete | None = None,
        never_drop_input: Incomplete | None = None,
        prime_output_buffers_using_stream_callback: Incomplete | None = None,
    ) -> None: ...
    @property
    def read_available(self): ...
    def read(self, frames): ...

class RawOutputStream(_StreamBase):
    def __init__(
        self,
        samplerate: Incomplete | None = None,
        blocksize: Incomplete | None = None,
        device: Incomplete | None = None,
        channels: Incomplete | None = None,
        dtype: Incomplete | None = None,
        latency: Incomplete | None = None,
        extra_settings: Incomplete | None = None,
        callback: Incomplete | None = None,
        finished_callback: Incomplete | None = None,
        clip_off: Incomplete | None = None,
        dither_off: Incomplete | None = None,
        never_drop_input: Incomplete | None = None,
        prime_output_buffers_using_stream_callback: Incomplete | None = None,
    ) -> None: ...
    @property
    def write_available(self): ...
    def write(self, data): ...

class RawStream(RawInputStream, RawOutputStream):
    def __init__(
        self,
        samplerate: Incomplete | None = None,
        blocksize: Incomplete | None = None,
        device: Incomplete | None = None,
        channels: Incomplete | None = None,
        dtype: Incomplete | None = None,
        latency: Incomplete | None = None,
        extra_settings: Incomplete | None = None,
        callback: Incomplete | None = None,
        finished_callback: Incomplete | None = None,
        clip_off: Incomplete | None = None,
        dither_off: Incomplete | None = None,
        never_drop_input: Incomplete | None = None,
        prime_output_buffers_using_stream_callback: Incomplete | None = None,
    ) -> None: ...

class InputStream(RawInputStream):
    def __init__(
        self,
        samplerate: Incomplete | None = None,
        blocksize: Incomplete | None = None,
        device: Incomplete | None = None,
        channels: Incomplete | None = None,
        dtype: Incomplete | None = None,
        latency: Incomplete | None = None,
        extra_settings: Incomplete | None = None,
        callback: Incomplete | None = None,
        finished_callback: Incomplete | None = None,
        clip_off: Incomplete | None = None,
        dither_off: Incomplete | None = None,
        never_drop_input: Incomplete | None = None,
        prime_output_buffers_using_stream_callback: Incomplete | None = None,
    ) -> None: ...
    def read(self, frames): ...

class OutputStream(RawOutputStream):
    def __init__(
        self,
        samplerate: Incomplete | None = None,
        blocksize: Incomplete | None = None,
        device: Incomplete | None = None,
        channels: Incomplete | None = None,
        dtype: Incomplete | None = None,
        latency: Incomplete | None = None,
        extra_settings: Incomplete | None = None,
        callback: Incomplete | None = None,
        finished_callback: Incomplete | None = None,
        clip_off: Incomplete | None = None,
        dither_off: Incomplete | None = None,
        never_drop_input: Incomplete | None = None,
        prime_output_buffers_using_stream_callback: Incomplete | None = None,
    ) -> None: ...
    def write(self, data): ...

class Stream(InputStream, OutputStream):
    def __init__(
        self,
        samplerate: Incomplete | None = None,
        blocksize: Incomplete | None = None,
        device: Incomplete | None = None,
        channels: Incomplete | None = None,
        dtype: Incomplete | None = None,
        latency: Incomplete | None = None,
        extra_settings: Incomplete | None = None,
        callback: Incomplete | None = None,
        finished_callback: Incomplete | None = None,
        clip_off: Incomplete | None = None,
        dither_off: Incomplete | None = None,
        never_drop_input: Incomplete | None = None,
        prime_output_buffers_using_stream_callback: Incomplete | None = None,
    ) -> None: ...

class DeviceList(tuple): ...

class CallbackFlags:
    def __init__(self, flags: int = 0) -> None: ...
    def __bool__(self) -> bool: ...
    def __ior__(self, other): ...
    @property
    def input_underflow(self): ...
    @input_underflow.setter
    def input_underflow(self, value) -> None: ...
    @property
    def input_overflow(self): ...
    @input_overflow.setter
    def input_overflow(self, value) -> None: ...
    @property
    def output_underflow(self): ...
    @output_underflow.setter
    def output_underflow(self, value) -> None: ...
    @property
    def output_overflow(self): ...
    @output_overflow.setter
    def output_overflow(self, value) -> None: ...
    @property
    def priming_output(self): ...

class _InputOutputPair:
    def __init__(self, parent, default_attr) -> None: ...
    def __getitem__(self, index): ...
    def __setitem__(self, index, value) -> None: ...

class default:
    device: Incomplete
    channels: Incomplete
    dtype: Incomplete
    latency: Incomplete
    extra_settings: Incomplete
    samplerate: Incomplete
    blocksize: Incomplete
    clip_off: bool
    dither_off: bool
    never_drop_input: bool
    prime_output_buffers_using_stream_callback: bool
    def __init__(self) -> None: ...
    def __setattr__(self, name, value) -> None: ...
    @property
    def hostapi(self): ...
    def reset(self) -> None: ...

class PortAudioError(Exception): ...
class CallbackStop(Exception): ...
class CallbackAbort(Exception): ...

class AsioSettings:
    def __init__(self, channel_selectors) -> None: ...

class CoreAudioSettings:
    def __init__(
        self,
        channel_map: Incomplete | None = None,
        change_device_parameters: bool = False,
        fail_if_conversion_required: bool = False,
        conversion_quality: str = "max",
    ) -> None: ...

class WasapiSettings:
    def __init__(self, exclusive: bool = False, auto_convert: bool = False) -> None: ...

class _CallbackContext:
    blocksize: Incomplete
    data: Incomplete
    out: Incomplete
    frame: int
    input_channels: Incomplete
    output_channels: Incomplete
    input_dtype: Incomplete
    output_dtype: Incomplete
    input_mapping: Incomplete
    output_mapping: Incomplete
    silent_channels: Incomplete
    loop: Incomplete
    event: Incomplete
    status: Incomplete
    def __init__(self, loop: bool = False) -> None: ...
    def check_data(self, data, mapping, device): ...
    def check_out(self, out, frames, channels, dtype, mapping): ...
    def callback_enter(self, status, data) -> None: ...
    def read_indata(self, indata) -> None: ...
    def write_outdata(self, outdata) -> None: ...
    def callback_exit(self) -> None: ...
    def finished_callback(self) -> None: ...
    stream: Incomplete
    def start_stream(
        self, StreamClass, samplerate, channels, dtype, callback, blocking, **kwargs
    ) -> None: ...
    def wait(self, ignore_errors: bool = True): ...
