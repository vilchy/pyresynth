<!--
SPDX-FileCopyrightText: 2019, 2023 Artur Wilniewczyc <code@artewil.com>

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# pyresynth

A simple python package for sound analysis.

Work in progress…

## Dependencies

1. [`sounddevice`](https://python-sounddevice.readthedocs.io/en/0.5.0/) module requires
   [PortAudio](https://www.portaudio.com/) (see: [`sounddevice` installation](https://python-sounddevice.readthedocs.io/en/0.5.0/installation.html)).

## Installation

You can download or clone the repository and use `pip` to handle dependencies:

```shell
unzip pyresynth.zip
pip install -e pyresynth
```

or

```shell
git clone https://codeberg.org/vilchy/pyresynth.git
pip install -e pyresynth
```

## Example use

```python
from pyresynth import Sample, TimeFrequency

# read a WAV file and play it
song = Sample.load("the_liberty_bell.wav")
song.play()

# generate sin + noise signal
s1 = Sample.generate_sin(294, 1)  # 294 Hz for 1 second
s2 = Sample.generate_white_noise(1, 1)  # for 1 second
s3 = s1 * 0.5 + s2 * 0.25

# save the results to a WAV file
s3.save("out_signal.wav")

# plot RMS amplitude envelope
e = s3.envelope_rms(150)
e.plot()

# return time-frequency representation
tf = TimeFrequency.stft(s3)

# plot spectrogram
tf.plot_spectrogram()
```

## Running tests

```shell
python3 -m pytest tests
```

## How to get help

Create [an issue](https://codeberg.org/vilchy/pyresynth/issues) on Codeberg.

## License

All original source code is licensed under the [MIT license](LICENSES/MIT.txt).
All documentation is licensed under
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
Some configuration files are licensed under
[Creative Commons Zero 1.0 Universal Public Domain](https://creativecommons.org/publicdomain/zero/1.0/).

For more accurate information, check the individual files.
