# SPDX-FileCopyrightText: 2019, 2023 Artur Wilniewczyc <code@artewil.com>
#
# SPDX-License-Identifier: CC0-1.0

matplotlib>=3.9.2
numpy==1.26.4
scipy>=1.14.1
sounddevice>=0.5.0
pytest
mypy
