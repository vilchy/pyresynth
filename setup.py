# SPDX-FileCopyrightText: 2019 Artur Wilniewczyc <code@artewil.com>
#
# SPDX-License-Identifier: MIT

# -*- coding: utf-8 -*-


from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

setup(
    name='pyresynth',
    version='0.0.1',
    description='Package for sound analysis',
    long_description=readme,
    author='Artur Wilniewczyc',
    author_email='code@artewil.com',
    url='https://codeberg.org/vilchy/pyresynth',
    license='MIT',
    install_requires=[
        'matplotlib >= 3.9.2',
        'numpy == 1.26.4',
        'scipy >= 1.14.1',
        'sounddevice >= 0.5.0',
    ],
    packages=find_packages(exclude=('tests', 'docs'))
)
